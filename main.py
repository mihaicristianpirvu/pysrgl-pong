from pong import Pong

def main():
    pong = Pong()
    print(pong)
    pong.run()

if __name__ == "__main__":
    main()
