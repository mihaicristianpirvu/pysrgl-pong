import numpy as np
from pathlib import Path
from pysrgl import Scene
from pysrgl.utils.keyboard import keyPressAndRelease, keyPress
from pysrgl.objects import OneColorObject, TexturedObject, Text, FPSText
from pysrgl.objects.primitive_objects import circle, horizontalLine, verticalLine

class PalletLeft(OneColorObject):
    def __init__(self):
        self.startPosition = 0.0, 0.5
        self.length = 0.15
        self.thickness = 0.015
        self.line = verticalLine(self.startPosition, self.length, self.thickness)
        self.color = (0, 0, 255)
        super().__init__(self.line, self.color)

class PalletRight(OneColorObject):
    def __init__(self):
        self.startPosition = 0.985, 0.5
        self.length = 0.15
        self.thickness = 0.015
        self.line = verticalLine(self.startPosition, self.length, self.thickness)
        self.color = (255, 0, 0)
        super().__init__(self.line, self.color)

class Ball(TexturedObject):
    def __init__(self):
        self.center = 0.5, 0.5
        self.radius = 0.02
        self.numSegments = 20
        self.circle = circle(self.center, self.radius, numSegments=self.numSegments)
        uv = np.array(circle((0.5, 0.5), 0.5, numSegments=self.numSegments)["vertex"], dtype=np.float32)[..., 0 : 2]
        # uv[:, 1] = 1 - uv[:, 1]
        super().__init__(self.circle, textureUV=uv, textureName="ball")

class TopWall(OneColorObject):
    def __init__(self):
        self.startPosition = 0, 0.06
        self.length = 1
        self.thickness = 0.007
        self.color = (0, 0, 255)
        self.line = horizontalLine(self.startPosition, self.length, self.thickness)
        super().__init__(self.line, color=self.color)

class Pong(Scene):
    def __init__(self):
        super().__init__(windowSize = (1280, 720))
        self.callback = self
        self.palletLeft = PalletLeft()
        self.palletRight = PalletRight()
        self.ball = Ball()
        self.topWall = TopWall()

        self.addTexture("ball", Path(__file__).parent.absolute() / "ball.png")
        polygons = {
            "ball" : self.ball,
            "topLine": self.topWall,
            "palletLeft": self.palletLeft,
            "palletRight": self.palletRight,
            "fps": FPSText((0, 0.05), scale = 0.001, color = (0, 0, 255)),
            "score": Text("", (0.65, 0.05), scale = 0.001, color = (0, 0, 255))
        }
        for name, polygon in polygons.items():
            self.addObject(name, polygon)

        # Define some objects we're always gonna use
        self.score = (0, 0)
        self.initializer()

    @property
    def angle(self):
        return self._angle
    
    @angle.setter
    def angle(self, _angle: float):
        assert -90 <= _angle <= 90
        self._angle = _angle

    def update(self):
        if keyPress(self, "ESC"):
            exit()
        if keyPressAndRelease(self, "R"):
            self.initializer()

        self.polygons["score"].text = f"Score: {self.score[0]} / {self.score[1]}"
        self.movePallets()
        self.moveBall()

    def initializer(self):
        self.palletLeft.resetPosition()
        self.palletRight.resetPosition()
        self.ball.resetPosition()

        assert 0 < self.ball.center[0] < 1 and 0 < self.ball.center[1] < 1

        # Move it slightly below so it's centered properly.
        self.palletLeft.translate(0, -self.palletLeft.length / 2, 0)
        self.palletRight.translate(0, -self.palletRight.length / 2, 0)
        # Constants
        self.ball.speed = 0.01
        self.palletSpeed = 0.015
        self.ballUpdateSpeedPercent = 0.1
        # Variables
        self.ball.angle = 0
        self.ball.directionX = 1
        self.ball.directionY = 1

    def computeBallNextPosition(self):
        # x^2 + y^2 = speed^2
        # tan(angle) = y / x
        # => x^2 + [tan(angle)^2 * x^2] = speed^2
        # => x^2 * (tan(angle)^2 + 1) = speed^2
        # => x = sqrt(speed^2 / [tan(angle)^2 + 1])
        # ==> x = speed / sqrt(tan(angle)^2 + 1)
        # ==> y = sqrt(speed^2 - x^2)

        _angle = np.radians(self.ball.angle)
        Denom = np.sqrt(np.tan(_angle)**2 + 1)
        dx = self.ball.speed / Denom
        dy = np.sqrt(self.ball.speed**2 - dx**2)

        dx = dx * self.ball.directionX
        dy = dy * self.ball.directionY
        return dx, dy

    def moveBall(self):
        dx, dy = self.computeBallNextPosition()
        self.ball.translate(dx, dy, 0)

        def hitLeftPallet(pallet, ball) -> bool:
            palletYStart = pallet.startPosition[1] + pallet.dy
            palletYEnd = palletYStart + pallet.length
            ballY = ball.center[1] + ball.dy
            ballX = ball.center[0] + ball.dx - ball.radius 
            return ballX <= pallet.thickness and palletYStart - ball.radius <= ballY <= palletYEnd + ball.radius
        def hitRightPallet(pallet, ball) -> bool:
            palletYStart = pallet.startPosition[1] + pallet.dy
            palletYEnd = palletYStart + pallet.length
            ballY = ball.center[1] + ball.dy
            ballX = ball.center[0] + ball.dx + ball.radius
            return ballX >= 1 - pallet.thickness and palletYStart - ball.radius <= ballY <= palletYEnd + ball.radius
        def hitLeftWall(ball) -> bool:
            return ball.center[0] + ball.dx - ball.radius <= 0
        def hitRightWall(ball) -> bool:
            return ball.center[0] + ball.dx + ball.radius >= 1
        def hitTopWall(ball, topWall) -> bool:
            return ball.center[1] + ball.dy - ball.radius <= topWall.startPosition[1] + topWall.thickness
        def hitBottomWall(ball) -> bool:
           return ball.center[0] + ball.dy + ball.radius >= 1

        def computeAngle(pallet, ball) -> float:
            """
            Given the pallet's position and ball's position, compute from top to bottom the percent and then, based
            on that percent, compute the angle, as [-45:45].
            """
            top = pallet.startPosition[1] + pallet.dy - ball.radius
            bottom = top + pallet.length + 2 * ball.radius
            ballPosition = ball.center[1] + ball.dy
            ballPosition -= top
            # [0 : 1]
            percent = ballPosition / (bottom - top)
            # [-45 : 45]
            angle = (percent * 180 - 90) / 2
            return angle

        if hitLeftPallet(self.palletLeft, self.ball):
            self.ball.directionX = 1
            self.ball.angle = computeAngle(self.palletLeft, self.ball)
            self.ball.directionY = np.sign(self.ball.angle)
            self.ball.speed += self.ballUpdateSpeedPercent * self.ball.speed
            return
        if hitRightPallet(self.palletRight, self.ball):
            self.ball.directionX = -1
            self.ball.angle = computeAngle(self.palletRight, self.ball)
            self.ball.directionY = np.sign(self.ball.angle)
            self.ball.speed += self.ballUpdateSpeedPercent * self.ball.speed
            return
        if hitTopWall(self.ball, self.topWall):
            self.ball.directionY = 1
            return
        if hitBottomWall(self.ball):
            self.ball.directionY = -1
            return
        if hitLeftWall(self.ball):
            self.initializer()
            self.score = (self.score[0], self.score[1] + 1)
            return
        if hitRightWall(self.ball):
            self.score = (self.score[0] + 1, self.score[1])
            self.initializer()
            return

    def movePallets(self):
        if keyPress(self, "UP"):
            palletTop = self.palletLeft.dy + self.palletLeft.startPosition[1]
            if palletTop >= self.topWall.startPosition[1] + self.topWall.thickness:
                self.palletLeft.translate(0, -self.palletSpeed, 0)
        if keyPress(self, "DOWN"):
            palletBottom = self.palletLeft.dy + self.palletLeft.startPosition[1] + self.palletLeft.length
            if palletBottom <= 1:
                self.palletLeft.translate(0, self.palletSpeed, 0)
        if keyPress(self, "Y"):
            palletTop = self.palletRight.dy + self.palletRight.startPosition[1]
            if palletTop >= self.topWall.startPosition[1] + self.topWall.thickness:
                self.palletRight.translate(0, -self.palletSpeed, 0)
        if keyPress(self, "H"):
            palletBottom = self.palletRight.dy + self.palletRight.startPosition[1] + self.palletRight.length
            if palletBottom <= 1:
                self.palletRight.translate(0, self.palletSpeed, 0)
    
    def __call__(self, *args, **kwargs):
        self.update()

    def __str__(self) -> str:
        numVertices, numIndices = 0, 0
        for polygon in self.polygons.values():
            numVertices += polygon.getNumVertices()
            numIndices += polygon.getNumIndices()

        Str = "[Pong]"
        Str += f"\n - Window size: {self.height}x{self.width}"
        Str += f"\n - Pallet speed: {self.palletSpeed}"
        Str += f"\n - Ball speed: {self.ball.speed}"
        Str += f"\n - Ball speed update per hit: {self.ballUpdateSpeedPercent * 100}%"
        return Str
